#pragma once
#include<SDL.h>
#include<string>

using namespace std;

class SDLManager
{
private:
	unsigned int screenSizeX;
	unsigned int screenSizeY;

	bool isRunning;
	bool isKeyDown;
	bool error;

	bool mouseLeftClick;
	bool spaceClick;

	bool mouseState[3] = { false, false, false };
	int MouseX;
	int MouseY;
	const unsigned char* keyboard;

	SDL_Event events;
	SDL_Window* win;

	SDL_Renderer* graphics;

	void InitSDL();
	void InitWin(const string& title, int winX, int winY);
	void InitRend();
public:
	SDLManager();
	void InitWinRendSDL(const string& title, int winX, int winY);
	void CheckEvents();
	void Close();
	void Clear(unsigned char r, unsigned char g, unsigned char b);
	void Render();

	int TakeSreenSizeX();
	int TakeScreenSizeY();

	bool TakeMouseLeftState();
	int TakeLocalMousePosX(int echelle);
	int TakeLocalMousePosY(int echelle);

	char CheckKeyBoard();

	bool CheckError();
	bool CheckIsRunning();

	SDL_Renderer* TakeGraphics();
};