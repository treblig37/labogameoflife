#include<windows.h>
#include"SDLManager.h"
#include"Clock.h"
#include"Grid.h"



int WINAPI WinMain(HINSTANCE h, HINSTANCE i, LPSTR c, int n)
{
	SDLManager gameManager;
	Clock clock(60);
	Grid grid;

	gameManager.InitWinRendSDL("GOL", 1800, 1000);
	if (gameManager.CheckError())
	{
		return 1;
	}

	//grid.SetGrid(6, 6, 32, 32);
	grid.SetGridWinSize(32, 32, gameManager.TakeSreenSizeX(), gameManager.TakeScreenSizeY());
	grid.SetColorDrawRect(0,0,0);
	grid.SetColorFillRect(236,245,66);
	grid.SetTime(0.2, clock.TakeFPS());
	
	while (gameManager.CheckIsRunning())
	{
		clock.SetStart();
		grid.IncrementTime();

		gameManager.CheckEvents();

		gameManager.Clear(65, 65, 65);

		grid.SetIsPlay(gameManager.CheckKeyBoard() == ' ');

		grid.SetCelluleState(gameManager.TakeLocalMousePosX(grid.TakeCellWidth()), gameManager.TakeLocalMousePosY(grid.TakeCellHeight()), gameManager.TakeMouseLeftState());
		grid.DrawGrid(gameManager.TakeGraphics());

		gameManager.Render();

		clock.SetEnd();
		clock.Wait();
	}

	gameManager.Close();
	return 0;
}