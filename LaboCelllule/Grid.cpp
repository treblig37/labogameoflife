#include"Grid.h"
#include<SDL.h>

void Grid::SetGrid(int gridWidth, int gridHeight, int cellWidth, int cellHeight)
{
	this->gridWidth = gridWidth;
	this->gridHeight = gridHeight;
	this->cellWidth = cellWidth;
	this->cellHeight = cellHeight;

	grid.resize(gridWidth * gridHeight);

	for (int i = 0; i < grid.size(); i++)
	{
		grid.at(i) = -1;
	}
}

void Grid::SetGridWinSize(int cellWidth, int cellHeight, int winX, int winY)
{
	this->cellWidth = cellWidth;
	this->cellHeight = cellHeight;
	this->gridWidth = winX / cellWidth + 1;
	this->gridHeight = winY / cellHeight + 1;

	grid.resize(gridWidth * gridHeight);

	for (int i = 0; i < grid.size(); i++)
	{
		grid.at(i) = -1;
	}
}

void Grid::DrawGrid(SDL_Renderer* graphics)
{
	SDL_SetRenderDrawColor(graphics, 0, 0, 0, 255);

	for (size_t i = 0; i < grid.size(); i++)
	{
		int x = i % gridWidth * cellWidth + offsetX;
		int y = i / gridWidth * cellHeight + offsetY;

		SDL_Rect rect = { x, y, cellWidth, cellHeight };

		if (grid[i] == -1)
		{
			SDL_SetRenderDrawColor(graphics, rgbDR[0], rgbDR[1], rgbDR[2], 255);
			SDL_RenderDrawRect(graphics, &rect);
		}
		else
		{
			SDL_SetRenderDrawColor(graphics, rgbFR[0], rgbFR[1], rgbFR[2], 255);
			SDL_RenderFillRect(graphics, &rect);
		}
	}
}

void Grid::SetCelluleState(int localX, int localY, bool isClick)
{
	bool nFrame = nextFrame;
	std::vector<int> gridTemp = grid;
	for (size_t i = 0; i < grid.size(); i++)
	{
		int x = i % gridWidth * cellWidth + offsetX;
		int y = i / gridWidth * cellHeight + offsetY;

		if (!isPlay)
		{
			if (x / cellWidth == localX && y / cellHeight == localY && isClick)
			{
				grid.at(i) = -grid.at(i);
			}
		}
		else if(nFrame)
		{

			//int nbAround = TakeNumberCellAround(x / cellWidth, y / cellHeight);
			int nbAround = TakeNumberCellAround2(x / cellWidth, y / cellHeight, i);

			if (grid[i] == 1)
			{
				if (nbAround != 2 && nbAround != 3)
				{
					gridTemp[i] = -gridTemp[i];
				}
			}
			else if (grid[i] == -1)
			{
				if (nbAround == 3)
				{
					gridTemp[i] = -gridTemp[i];
				}
			}
		}

	}
	if (isPlay)
	{
		grid = gridTemp;
	}
	nextFrame = false;
}

void Grid::SetOffset(int x, int y)
{
	offsetX = x;
	offsetY = y;
}

void Grid::SetColorFillRect(int r, int g, int b)
{
	rgbFR[0] = r;
	rgbFR[1] = g;
	rgbFR[2] = b;
}

void Grid::SetColorDrawRect(int r, int g, int b)
{
	rgbDR[0] = r;
	rgbDR[1] = g;
	rgbDR[2] = b;
}

void Grid::SetIsPlay(bool click)
{
	if (click)
	{
		if (!isPlay)
		{
			isPlay = true;
		}
		else if (isPlay)
		{
			isPlay = false;
		}
	}

}

void Grid::SetTime(float t, int fps)
{
	time = t * fps;
	incrementTime = 1;
}

void Grid::IncrementTime()
{
	currTime += incrementTime;
	if (currTime >= time)
	{
		nextFrame = true;
		currTime = 0;
	}
}

int Grid::TakeNumberCellAround(int x, int y)
{
	int tablX[] = { x - 1 , x + 1, x, x, x + 1, x - 1, x + 1, x - 1 };
	int tablY[] = { y, y, y - 1, y + 1, y + 1, y - 1, y - 1, y + 1 };

	int nbAround = 0;

	for (size_t i = 0; i < grid.size(); i++)
	{
		int x2 = i % gridWidth * cellWidth + offsetX;
		int y2 = i / gridWidth * cellHeight + offsetY;

		x2 /= cellWidth;
		y2 /= cellHeight;

		for (int a = 0; a < 9; a++)
		{
			if (x2 == tablX[a] && y2 == tablY[a])
			{
				if (grid[i] == 1)
				{
					nbAround++;
				}
			}
		}
	}

	return nbAround;
}

int Grid::TakeNumberCellAround2(int x, int y, int position)
{
	int nbAround = 0;

	if (x != 0 && y != 0 && x != gridWidth - 1 && y != gridHeight - 1)
	{
		int firstPostion = position - gridWidth - 1;
		int lastPosition = position + gridWidth + 1;
		int b = 0;
		for (int i = firstPostion; i <= lastPosition; i++)
		{
			if (b == 3)
			{
				i += gridWidth - 3;
				b = 0;
			}

			if (grid[i] == 1 && i != position)
			{
				nbAround++;
			}

			b++;
		}
	}
	else if ((x == 0 && y != 0 && y != gridHeight - 1) || (x == gridWidth - 1 && y != 0 && y != gridHeight - 1))
	{
		if ((x == 0 && y != 0 && y != gridHeight - 1))
		{
			int firstPostion = position - gridWidth;
			int lastPosition = position + gridWidth + 1;
			int b = 0;
			for (int i = firstPostion; i <= lastPosition; i++)
			{
				if (b == 2)
				{
					i += gridWidth - 2;
					b = 0;
				}

				if (grid[i] == 1 && i != position)
				{
					nbAround++;
				}

				b++;
			}
		}
		else if (x == gridWidth - 1 && y != 0 && y != gridHeight - 1)
		{
			int firstPostion = position - gridWidth - 1;
			int lastPosition = position + gridWidth;
			int b = 0;
			for (int i = firstPostion; i <= lastPosition; i++)
			{
				if (b == 2)
				{
					i += gridWidth - 2;
					b = 0;
				}

				if (grid[i] == 1 && i != position)
				{
					nbAround++;
				}

				b++;
			}
		}
	}
	else if ((y == 0 && x != 0 && x != gridWidth - 1) || (y == gridHeight - 1 && x != 0 && x != gridWidth - 1))
	{
		if ((y == 0 && x != 0 && x != gridWidth - 1))
		{
			int firstPostion = position - 1;
			int lastPosition = position + gridWidth + 1;
			int b = 0;
			for (int i = firstPostion; i <= lastPosition; i++)
			{
				if (b == 3)
				{
					i += gridWidth - 3;
					b = 0;
				}

				if (grid[i] == 1 && i != position)
				{
					nbAround++;
				}

				b++;
			}
		}
		else if ((y == gridHeight - 1 && x != 0 && x != gridWidth - 1))
		{
			int firstPostion = position - gridWidth - 1;
			int lastPosition = position + 1;
			int b = 0;
			for (int i = firstPostion; i <= lastPosition; i++)
			{
				if (b == 3)
				{
					i += gridWidth - 3;
					b = 0;
				}

				if (grid[i] == 1 && i != position)
				{
					nbAround++;
				}

				b++;
			}
		}
	}
	else if ((x == 0 && y == 0) || (x == 0 && y == gridHeight - 1) || (x == gridWidth - 1 && y == 0) || (x == gridWidth - 1 && y == gridHeight - 1))
	{
		if(x == 0 && y == 0)
		{
			int firstPostion = position;
			int lastPosition = position + gridWidth + 1;
			int b = 0;
			for (int i = firstPostion; i <= lastPosition; i++)
			{
				if (b == 2)
				{
					i += gridWidth - 2;
					b = 0;
				}

				if (grid[i] == 1 && i != position)
				{
					nbAround++;
				}

				b++;
			}
		}
		else if (x == 0 && y == gridHeight - 1)
		{
			int firstPostion = position - gridWidth;
			int lastPosition = position + 1;
			int b = 0;
			for (int i = firstPostion; i <= lastPosition; i++)
			{
				if (b == 2)
				{
					i += gridWidth - 2;
					b = 0;
				}

				if (grid[i] == 1 && i != position)
				{
					nbAround++;
				}

				b++;
			}
		}
		else if (x == gridWidth - 1 && y == 0)
		{
			int firstPostion = position - 1;
			int lastPosition = position + gridWidth;
			int b = 0;
			for (int i = firstPostion; i <= lastPosition; i++)
			{
				if (b == 2)
				{
					i += gridWidth - 2;
					b = 0;
				}

				if (grid[i] == 1 && i != position)
				{
					nbAround++;
				}

				b++;
			}
		}
		else if (x == gridWidth - 1 && y == gridHeight - 1)
		{
			int firstPostion = position - gridWidth - 1;
			int lastPosition = position;
			int b = 0;
			for (int i = firstPostion; i <= lastPosition; i++)
			{
				if (b == 2)
				{
					i += gridWidth - 2;
					b = 0;
				}

				if (grid[i] == 1 && i != position)
				{
					nbAround++;
				}

				b++;
			}
		}
	}


	return nbAround;
}

int Grid::TakeCellWidth()
{
	return cellWidth;
}

int Grid::TakeCellHeight()
{
	return cellHeight;
}