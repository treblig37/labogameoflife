#pragma once
#include <vector>

struct SDL_Renderer;

class Grid
{
private:

	std::vector<int> grid;
	int gridWidth;
	int gridHeight;
	int cellWidth;
	int cellHeight;

	int rgbFR[3] = {0};
	int rgbDR[3] = {0};

	int offsetX = 0;
	int offsetY = 0;

	float time = 0;
	float currTime = 0;
	float incrementTime = 0;
	bool nextFrame = true;

	bool isPlay = false;
public:

	void SetGrid(int gridWidth, int gridHeight, int cellWidth, int cellHeight);
	void SetGridWinSize(int cellWidth, int cellHeight, int winX, int winY);
	void DrawGrid(SDL_Renderer* graphics);
	void SetCelluleState(int localX, int localY, bool isClick);
	void SetOffset(int x, int y);
	void SetColorFillRect(int r, int g, int b);
	void SetColorDrawRect(int r, int g, int b);
	void SetIsPlay(bool click);
	void SetTime(float t, int fps);
	void IncrementTime();
	
	int TakeNumberCellAround(int x, int y);
	int TakeNumberCellAround2(int x, int y, int position);

	int TakeCellWidth();
	int TakeCellHeight();
};