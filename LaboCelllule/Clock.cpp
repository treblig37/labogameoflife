#include"Clock.h"
#include<windows.h>

Clock::Clock() : Clock(60)
{

}

Clock::Clock(unsigned int FPS)
{
	SetFPS(FPS);
}

void Clock::SetFPS(unsigned int FPS)
{
	this->FPS = FPS;
	FRAME_TARGET_TIME = 1000 / FPS;
}

void Clock::SetStart()
{
	start = clock();
}

void Clock::SetEnd()
{
	end = clock();
}

void Clock::Wait()
{
	rest = start + FRAME_TARGET_TIME - end;

	if (rest > 0)
	{
		Sleep(rest);
	}
}

int Clock::TakeFPS()
{
	return FPS;
}
