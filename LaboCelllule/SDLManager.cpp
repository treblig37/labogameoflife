#include<string>
#include"SDLManager.h"

using namespace std;

SDLManager::SDLManager()
{
	isRunning = true;
	isKeyDown = false;
	error = false;
	keyboard = nullptr;
	MouseX = 0;
	MouseY = 0;
	mouseLeftClick = false;
	spaceClick = false;
}

void SDLManager::InitWinRendSDL(const string& title, int winX, int winY)
{
	InitSDL();
	if (CheckError())
	{
		return;
	}
	InitWin(title, winX, winY);
	if (CheckError())
	{
		return;
	}
	InitRend();
}

void SDLManager::InitSDL()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0)
	{
		SDL_Log("Error : %s\n", SDL_GetError());
		error = true;
	}
}

void SDLManager::InitWin(const string& title, int winX, int winY)
{
	screenSizeX = winX;
	screenSizeY = winY;

	win = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, winX, winY, SDL_WINDOW_UTILITY);
	if (win == nullptr)
	{
		SDL_Log("Error : %s\n", SDL_GetError());
		error = true;
	}
}

void SDLManager::InitRend()
{
	graphics = SDL_CreateRenderer(win, 0, SDL_RENDERER_ACCELERATED);
	if (graphics == nullptr)
	{
		SDL_Log("Error : %s\n", SDL_GetError());
		error = true;
	}
}

void SDLManager::CheckEvents()
{
	SDL_PollEvent(&events);

	switch (events.type)
	{
	case SDL_QUIT:
		isRunning = false;
		break;

	case SDL_KEYDOWN:
	case SDL_KEYUP:
		keyboard = SDL_GetKeyboardState(nullptr);
		break;

	case SDL_MOUSEBUTTONDOWN:
		switch (events.button.button)
		{
		case SDL_BUTTON_LEFT:
			mouseState[0] = true;
			break;

		case SDL_BUTTON_MIDDLE:
			mouseState[1] = true;
			break;

		case SDL_BUTTON_RIGHT:
			mouseState[2] = true;
			break;
		}

		break;

	case SDL_MOUSEBUTTONUP:
		switch (events.button.button)
		{
		case SDL_BUTTON_LEFT:
			mouseState[0] = false;
			break;

		case SDL_BUTTON_MIDDLE:
			mouseState[1] = false;
			break;

		case SDL_BUTTON_RIGHT:
			mouseState[2] = false;
			break;
		}

		break;

	case SDL_MOUSEMOTION:
		MouseX = events.motion.x;
		MouseY = events.motion.y;
		break;
	}
}

void SDLManager::Close()
{
	SDL_DestroyRenderer(graphics);
	SDL_DestroyWindow(win);
	SDL_Quit();
}

void SDLManager::Clear(unsigned char r, unsigned char g, unsigned char b)
{
	SDL_SetRenderDrawColor(graphics, r, g, b, 255);
	SDL_RenderClear(graphics);
}

void SDLManager::Render()
{
	SDL_RenderPresent(graphics);
}

int SDLManager::TakeSreenSizeX()
{
	return screenSizeX;
}

int SDLManager::TakeScreenSizeY()
{
	return screenSizeY;
}

char SDLManager::CheckKeyBoard()
{
	if (keyboard == nullptr)
	{
		spaceClick = false;
		return 0;
	}
	if (keyboard[SDL_SCANCODE_SPACE] == 1 && !spaceClick)
	{
		spaceClick = true;
		return ' ';
	}
	else if (keyboard[SDL_SCANCODE_SPACE] == 0 && spaceClick)
	{
		spaceClick = false;
		return 0;
	}
	return 0;
}

bool SDLManager::TakeMouseLeftState()
{
	if (mouseState[0] == true && !mouseLeftClick)
	{
		mouseLeftClick = true;
		return true;
	}
	else if(mouseState[0] == false)
	{
		mouseLeftClick = false;
	}
	return false;
}

int SDLManager::TakeLocalMousePosX(int echelle)
{
	int localPos = MouseX / echelle;
	return localPos;
}

int SDLManager::TakeLocalMousePosY(int echelle)
{
	int localPos = MouseY / echelle;
	return localPos;
}

bool SDLManager::CheckError()
{
	return error;
}

bool SDLManager::CheckIsRunning()
{
	return isRunning;
}

SDL_Renderer* SDLManager::TakeGraphics()
{
	return graphics;
}