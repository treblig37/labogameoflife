#pragma once
#include<time.h>


class Clock
{
private:
	unsigned int FPS;
	unsigned int FRAME_TARGET_TIME;

	clock_t start;
	clock_t end;

	int rest;
public:
	Clock();
	Clock(unsigned int FPS);
	void SetFPS(unsigned int FPS);
	void SetStart();
	void SetEnd();
	void Wait();
	int TakeFPS();
};